// FINNISH text

//
// these are the key numbers that should be passed to Key_Event
//
#define	K_TAB			9
#define	K_ENTER			13
#define	K_ESCAPE		27
#define	K_SPACE			32
// normal keys should be passed as lowercased ascii
#define	K_BACKSPACE		127
#define	K_UPARROW		128
#define	K_DOWNARROW		129
#define	K_LEFTARROW		130
#define	K_RIGHTARROW		131
#define	K_ALT			132
#define	K_CTRL			133
#define	K_SHIFT			134
#define	K_F1			135
#define	K_F2			136
#define	K_F3			137
#define	K_F4			138
#define	K_F5			139
#define	K_F6			140
#define	K_F7			141
#define	K_F8			142
#define	K_F9			143
#define	K_F10			144
#define	K_F11			145
#define	K_F12			146
#define	K_INS			147
#define	K_DEL			148
#define	K_PGDN			149
#define	K_PGUP			150
#define	K_HOME			151
#define	K_END			152

#define	K_KP_HOME		160
#define	K_KP_UPARROW		161
#define	K_KP_PGUP		162
#define	K_KP_LEFTARROW		163
#define	K_KP_5			164
#define	K_KP_RIGHTARROW		165
#define	K_KP_END		166
#define	K_KP_DOWNARROW		167
#define	K_KP_PGDN		168
#define	K_KP_ENTER		169
#define	K_KP_INS		170
#define	K_KP_DEL		171
#define	K_KP_SLASH		172
#define	K_KP_MINUS		173
#define	K_KP_PLUS		174
#define	K_PAUSE			255
//
// mouse buttons generate virtual keys
//
#define	K_MOUSE1		200
#define	K_MOUSE2		201
#define	K_MOUSE3		202
#define	K_MOUSE4		203	// Knightmare added 5/29/12
#define	K_MOUSE5		204	// Knightmare added 5/29/12
//
// joystick buttons
//
#define	K_JOY1			205
#define	K_JOY2			206
#define	K_JOY3			207
#define	K_JOY4			208
//
// aux keys are for multi-buttoned joysticks to generate so they can use
// the normal binding process
//
#define	K_AUX1			209
#define	K_AUX2			210
#define	K_AUX3			211
#define	K_AUX4			212
#define	K_AUX5			213
#define	K_AUX6			214
#define	K_AUX7			215
#define	K_AUX8			216
#define	K_AUX9			217
#define	K_AUX10			218
#define	K_AUX11			219
#define	K_AUX12			220
#define	K_AUX13			221
#define	K_AUX14			222
#define	K_AUX15			223
#define	K_AUX16			224
#define	K_AUX17			225
#define	K_AUX18			226
#define	K_AUX19			227
#define	K_AUX20			228
#define	K_AUX21			229
#define	K_AUX22			230
#define	K_AUX23			231
#define	K_AUX24			232
#define	K_AUX25			233
#define	K_AUX26			234
#define	K_AUX27			235
#define	K_AUX28			236
#define	K_AUX29			237
#define	K_AUX30			238
#define	K_AUX31			239
#define	K_AUX32			240

#define	K_MWHEELDOWN		241
#define	K_MWHEELUP		242

typedef struct
{
	char	*name;
	int	keynum;
} keyname_t;

static keyname_t keynames[] =
{
	{"TAB", K_TAB},
	{"ENTER", K_ENTER},
	{"ESCAPE", K_ESCAPE},
	{"SPACE", K_SPACE},
	{"BACKSPACE", K_BACKSPACE},
	{"UPARROW", K_UPARROW},
	{"DOWNARROW", K_DOWNARROW},
	{"LEFTARROW", K_LEFTARROW},
	{"RIGHTARROW", K_RIGHTARROW},

	{"ALT", K_ALT},
	{"CTRL", K_CTRL},
	{"SHIFT", K_SHIFT},

	{"F1", K_F1},
	{"F2", K_F2},
	{"F3", K_F3},
	{"F4", K_F4},
	{"F5", K_F5},
	{"F6", K_F6},
	{"F7", K_F7},
	{"F8", K_F8},
	{"F9", K_F9},
	{"F10", K_F10},
	{"F11", K_F11},
	{"F12", K_F12},

	{"INS", K_INS},
	{"DEL", K_DEL},
	{"PGDN", K_PGDN},
	{"PGUP", K_PGUP},
	{"HOME", K_HOME},
	{"END", K_END},

	{"MOUSE1", K_MOUSE1},
	{"MOUSE2", K_MOUSE2},
	{"MOUSE3", K_MOUSE3},
	{"MOUSE4", K_MOUSE4},	// Knightmare added 5/29/12
	{"MOUSE5", K_MOUSE5},	// Knightmare added 5/29/12

	{"JOY1", K_JOY1},
	{"JOY2", K_JOY2},
	{"JOY3", K_JOY3},
	{"JOY4", K_JOY4},

	{"AUX1", K_AUX1},
	{"AUX2", K_AUX2},
	{"AUX3", K_AUX3},
	{"AUX4", K_AUX4},
	{"AUX5", K_AUX5},
	{"AUX6", K_AUX6},
	{"AUX7", K_AUX7},
	{"AUX8", K_AUX8},
	{"AUX9", K_AUX9},
	{"AUX10", K_AUX10},
	{"AUX11", K_AUX11},
	{"AUX12", K_AUX12},
	{"AUX13", K_AUX13},
	{"AUX14", K_AUX14},
	{"AUX15", K_AUX15},
	{"AUX16", K_AUX16},
	{"AUX17", K_AUX17},
	{"AUX18", K_AUX18},
	{"AUX19", K_AUX19},
	{"AUX20", K_AUX20},
	{"AUX21", K_AUX21},
	{"AUX22", K_AUX22},
	{"AUX23", K_AUX23},
	{"AUX24", K_AUX24},
	{"AUX25", K_AUX25},
	{"AUX26", K_AUX26},
	{"AUX27", K_AUX27},
	{"AUX28", K_AUX28},
	{"AUX29", K_AUX29},
	{"AUX30", K_AUX30},
	{"AUX31", K_AUX31},
	{"AUX32", K_AUX32},

	{"KP_HOME", K_KP_HOME },
	{"KP_UPARROW", K_KP_UPARROW },
	{"KP_PGUP", K_KP_PGUP },
	{"KP_LEFTARROW", K_KP_LEFTARROW },
	{"KP_5", K_KP_5 },
	{"KP_RIGHTARROW", K_KP_RIGHTARROW },
	{"KP_END", K_KP_END },
	{"KP_DOWNARROW", K_KP_DOWNARROW },
	{"KP_PGDN", K_KP_PGDN },
	{"KP_ENTER", K_KP_ENTER },
	{"KP_INS", K_KP_INS },
	{"KP_DEL", K_KP_DEL },
	{"KP_SLASH", K_KP_SLASH },
	{"KP_MINUS", K_KP_MINUS },
	{"KP_PLUS", K_KP_PLUS },

	{"MWHEELUP", K_MWHEELUP },
	{"MWHEELDOWN", K_MWHEELDOWN },

	{"PAUSE", K_PAUSE},

	{"SEMICOLON", ';'}, // because a raw semicolon seperates commands

	{NULL, 0}
};

// vkey conversion table
//==========================================================================
static byte scantokey[128] = {
//	0		1	2		3		4	5		6		7
//	8		9	A		B		C	D		E		F 
	0,		27,	'1',		'2',		'3',	'4',		'5',		'6',
	'7',		'8',	'9',		'0',		'-',	'=',		K_BACKSPACE,	9,	// 0
	'q',		'w',	'e',		'r',		't',	'y',		'u',		'i',
	'o',		'p',	'[',		']',		13,	K_CTRL,		'a',		's',	// 1
	'd',		'f',	'g',		'h',		'j',	'k',		'l',		';',
	'\'',		'`',	K_SHIFT,	'\\',		'z',	'x',		'c',		'v',	// 2
	'b',		'n',	'm',		',',		'.',	'/',		K_SHIFT,	'*',
	K_ALT,		' ',	0  ,		K_F1,		K_F2,	K_F3,		K_F4,		K_F5,	// 3
	K_F6,		K_F7,	K_F8,		K_F9,		K_F10,	K_PAUSE,	0,		K_HOME,
	K_UPARROW,	K_PGUP,	K_KP_MINUS,	K_LEFTARROW,	K_KP_5,	K_RIGHTARROW,	K_KP_PLUS,	K_END,	// 4
	K_DOWNARROW,	K_PGDN,	K_INS,		K_DEL,		0,	0,		0,		K_F11,
	K_F12,		0,	0,		0,		0,	0,		0,		0,	// 5
	0,		0,	0,		0,		0,	0,		0,		0,
	0,		0,	0,		0,		0,	0,		0,		0,	// 6
	0,		0,	0,		0,		0,	0,		0,		0,
	0,		0,	0,		0,		0,	0,		0,		0	// 7
};

static char *tongue[] =
{
	"Poimit",					// T_PICKUP_WEAPON		weapon pickup
	"Tarvitsit",					// T_PICKUP_HEALTH		health pickup
	"Saat suojaa",					// T_PICKUP_ARMOR_GOOD		armor good pickup
	"Saat tarmoa",					// T_PICKUP_BOOST		boost pickup		TL Note: This is difficult to express in a way that makes sense.
	"L�ysit tallennustimantin",			// T_PICKUP_SAVEGEM		pick up Save Gem	TL Note: Instead of "Acquired" I used "Found".

	"VOIMAAN",					// T_USEBOOST_POWER		power boost label
	"HY�KK�YKSEEN",					// T_USEBOOST_ATTACK		attack boost label
	"NOPEUTEEN",					// T_USEBOOST_SPEED		speed boost label
	"AKROBATIAAN",					// T_USEBOOST_ACRO		jump boost label
	"ELINVOIMAAN",					// T_USEBOOST_VITA		health boost label

	"Aika pist�� t�pin�ksi",			// T_BOMB_EXPLODE		bomb explode		TL Note: Instead of "boogie" (dance?) I used "bustle". I could use "waltz" as well but it sounds bit clumsy imo.
	"Tarvitset pullon, johon sekoittaa sen",	// T_BOMB_NEED_BOTTLE		need bottle to mix ingredients
	"Tarvittavat esineet:",				// T_BOMB_ITEM_REQUIRED1	ingredient required
	"Loit r�j�hteen",				// T_BOMB_CREATED		bomb created
	"L�ysit",					// T_BOMB_FOUND			bomb found, vowel
	"pussillisen rikki�",				// T_BOMB_INGREDIENT_1		bomb ingredient 1
	"kokkareen hiilt�",				// T_BOMB_INGREDIENT_2		bomb ingredient 2
	"kemikaalipullollisen salpietaria",		// T_BOMB_INGREDIENT_3		bomb ingredient 3
	"tyhj�n pullon",				// T_BOMB_BOTTLE		bomb bottle

	"rikki�",					// T_BOMB_INGREDIENT_SHORT_1	bomb item, short description
	"puuhiilt�",					// T_BOMB_INGREDIENT_SHORT_2	bomb item, short description
	"salpietaria",					// T_BOMB_INGREDIENT_SHORT_3	bomb item, short description

	"Luet",						// T_BOOK_READ			read book
	"Et voi k�ytt�� t�t� juuri nyt",		// T_BOOK_NO_READ		no read book
	"Wyndraxin Loitsukirjaa",			// T_BOOK_WYNDRAX		Wyndrax spellbook

	"Poimit",					// T_KEY_PICKUP			key pickup
	"Hautaholvin avaimen",				// T_KEY_CRYPT			crypt key name
	"Wyndrax-avaimen",				// T_KEY_WYNDRAX		Wyndrax key name
	"vankisellin avainkortin",			// T_KEY_PRISONCELL		Prison cell key

	"punaisen avainkortin",				// T_KEY_RED_CARD				
	"sinisen avainkortin",				// T_KEY_BLUE_CARD				
	"vihre�n avainkortin",				// T_KEY_GREEN_CARD				
	"keltaisen avainkortin",			// T_KEY_YELLOW_CARD			
	"Hex-avainkiven",				// T_KEY_HEX_KEYSTONE			
	"Quad-avainkiven",				// T_KEY_QUAD_KEYSTONE			
	"Trigon-avainkiven",				// T_KEY_TRIGON_KEYSTONE		
	"Megakilven",					// T_MEGASHIELD					
	"Manakallon",					// T_SKULL_OF_INVINCIBILITY
	"Vastamyrkky�",					// T_POISON_ANTIDOTE
	"Drakman",					// T_DRACHMA
	"Charonin torven",				// T_HORN
	"AEGIS-avaimen osan (A)",			// T_KEY_AEGIS_A
	"AEGIS-avaimen osan (E)",			// T_KEY_AEGIS_E
	"AEGIS-avaimen osan (G)",			// T_KEY_AEGIS_G
	"AEGIS-avaimen osan (I)",			// T_KEY_AEGIS_I
	"AEGIS-avaimen osan (S)",			// T_KEY_AEGIS_S
	"Puhdistimen sirpaleen",			// T_PURIFIER_SHARD
	"Mustakirstu",					// T_BLACK_CHEST
	"Tulelta suojaava sormus",			// T_RING_OF_FIRE
	"Ep�kuolleilta suojaava sormus",		// T_RING_OF_UNDEAD
	"Salamaniskuilta suojaava sormus",		// T_RING_OF_LIGHTNING
	"Suojahaalarin",				// T_ENVIROSUIT

	"terveyspakkauksen",				// T_HEALTH_KIT
	"Kultaisensielun",				// T_GOLDENSOUL
	"Haamupallon",					// T_WRAITHORB,

	"tallennustimantti",				// T_SAVEGEM
	"Sinulla ei ole tallennustimantteja",		// cek[12-13-99]
	"l��kepullo",					// cek[1-3-00]

	// cek[2-2-00]: altered the following ***********************************************
	"terveyspakkaus",
	"el�m�vaasi",
	"terveyspullo",
	"terveyskori",
	"terveyslaatikko",
	"Vastamyrkky on loppu.\n",			// FS: Added CR

	// SCG[2/8/00]: messages for completed artifacts
	"L�ysit kaikki AEGIS-riimut!",
	"L�ysit kaikki kolme avainkive�!",
	"Olet koonnut Puhdistimen",
	"S�it hedelm�n.\n"				// FS
};


// menu-related
static char *tongue_menu[] =
{
	"Eteenp�in",		// key bind to Move Forward
	"Taaksep�in",		// key bind to Move Backward
	"Askel Vasemmalle",	// key bind to Step Left
	"Askel Oikealle",	// key bind to Step Right
	"Hy�kk��",		// key bind to Attack
	"Hypp��",		// key bind to Jump/Up
	"K�yt�",		// key bind to Use/Operate object
	"Kumarru",		// key bind to Crouch/Down
	"Seuraava Ase",		// key bind to Next Weapon
	"Edellinen Ase",	// key bind to Prev Weapon
	"K��nny Vasempaan",	// key bind to Turn Left
	"K��nny Oikeaan",	// key bind to Turn Right
	"Juokse",		// key bind to Run
	"Sivuttainastelu",	// key bind to Sidestep
	"Katso Yl�s",		// key bind to Look Up
	"Katso Alas",		// key bind to Look Down
	"Keskit� n�kym�",	// key bind to Center View
	"Hiirikatselu",		// key bind to Mouse Look
	"N�pp�inkatselu",	// key bind to Keyboard Look
	"Suurenna n�kym��",	// key bind to Screen size up
	"Pienenn� n�kym��",	// key bind to Screen size down
	"N�yt� kaikki tilan�kym�t", // key bind to Show All Huds	TL Note: In Finnish language "HUD" isn't in common use. Since HUD refers to "status displays" in DK I used "status displays".
	"Avaa Inv.",		// key bind to Inventory Open
	"K�yt� Inv. Esinett�",	// key bind to Use Item
	"Seuraava Esine",	// key bind to Next Item
	"Edellinen Esine",	// key bind to Prev Item
	"Apurin vaihto",	// key bind to Swap Sidekick
	"Seur. Komento",	// key bind to Command Next
	"Edl. Komento",		// key bind to Command Prev
	"Komenna",		// key bind to Command Apply	// cek[1-5-00] changed from Apply to Use	TL Note: This probably should be "order" instead of "use".

	ARROW_LEFT_STRING	" Vasen",	// left arrow key
	ARROW_UP_STRING		" Yl�s",	// up arrow key
	ARROW_DOWN_STRING	" Alas",	// down arrow key
	ARROW_RIGHT_STRING	" Oikea",	// right arrow key
				"--",		// no bind setting for this key

	"Klikkaa tai paina Enter asettaaksesi, DEL poistaa",	// set/delete key
	"Klikkaa tai paina Enter valitaksesi",			// select key
	"Paina n�pp�int� asettaaksesi, ESC peruu",		// set/abort
	
	"Oletusasetukset",		// default button
	
	"Ei mit��n",			// no mouse bindings
	"Liiku Vasemmalle/Oikealle",	// mouse "move left and right" setting
	"K��nny Vasemmalle/Oikealle",	// mouse "turn left and right" setting
	"Katso Yl�s/Alas",		// mouse "look up and down" setting
	"Liiku Eteenp�in/Taaksep�in",	// mouse "move forward and back" setting
	"K��nteinen Y-akseli",		// reverse mouse axis
	"Hiirell� katselu",		// Mouse-Look
	"Katselu-sivuttainastelu",	// Mouse-Look
	"X-akseli",		// x axis
	"Y-akseli",		// y axis
	"Painike 1",		// button 1 label
	"Painike 2",		// button 2 label
	"Painike 3",		// button 3 label
	"Valitse komento, ESC peruu",	// select a command, ESCape to cancel
	"K��nny V/O",		// turn left and right, abbreviated
	"Sivuttainastele V/O",	// strafe left and right, abbreviated
	"Katso Y/A",		// look up and down, abbreviated
	"Liiku E/T",		// move forward and back, abbreviated
	"Vaakasuuntainen herkkyys",	// mouse sensitivity for x-axis		//cek[12-13-99]
	"Pystysuuntainen herkkyys",	// mouse sensitivity for y-axis		//cek[12-13-99]
	"S��d� hiiren asetuksia:",	// configure mouse options

	"Kuolemaottelu",	// 'deathmatch' game
	"Kuolemahippa",		// 'deathtag' game
	"Yhteisty�",		// 'cooperative' game

	"Helppo",		// easy difficulty
	"Keskivaikea",		// medium difficulty
	"Vaikea",		// hard difficulty

	"Etsi Internet-peli",	// SCG[12/1/99]:
	"Etsi paikallinen peli",// SCG[12/1/99]:
	"K�ynnist� moninpeli",	// SCG[12/1/99]:

	"L�hiverkko",		// local area network game
	"Internet",		// internet game

	"Liity",		// join server
	"Is�nn�i",		// host new server
	"Lis�� " ARROW_DOWN_STRING,	// add address
	"Virkist�",		// refresh network games
	"Asetukset",		// player setup

	"Yhteystyyppi",		// connection speed label
	"28.8k",		// connection type 1
	"56.6k",		// connection type 2
	"ISDN",			// connection type 3
	"Kaapeli/DSL",		// connection type 4
	"T1/LAN",		// connection type 5
	"K�ytt�j�n m��rittelem�",		// connection type 6

	"Aikaraja",					// SCG[12/2/99]: 
	"Hahmon V�ri",					// SCG[12/2/99]: 
	"Aloitus Episoidi",				// SCG[12/2/99]: 
	"Joukkueen valinta",				// SCG[12/1/99]: 
	"CTF Rajoitus",					// SCG[12/1/99]: 
	"Joukkue1 V�ri",				// SCG[12/1/99]: 
	"Joukkue2 V�ri",				// SCG[12/1/99]: 
	"Tulosraja",					// SCG[12/1/99]: 
	"Tappopisteraja",				// SCG[12/1/99]: 
	"Tasoraja",					// level limit
	"Aloitus Episoidi",				// SCG[12/2/99]: 
	"Pelikartta",					// SCG[12/1/99]: 
	"Pelitila",					// game mode (deathmatch,deathtag,co-op)
	"Kykytaso",					// skill level
	"Aikaraja",					// time limit
	"Pelaajia Enim.",				// maximum players
	"Hahmot",					// character
	"Pohjav�ri",					// base color
	"K�ynnist�!",					// start multiplayer game

	"Aseet pysyv�sti saatavilla",			// weapons remain
	"Esineet uudelleen kehkeytyv�t",		// items rematerialize
	"Synny peliin kauimpana muista",		// spawn farthest point from players
	"Salli poistuminen",				// allow exiting of the level
	"Sama Kartta",					// same map
	"Pakota uudelleen syntyminen",			// force respawn
	"Putoamisvaurioit",				// players take falling damage
	"Voimat heti k�ytett�viss�",			// instant power-ups
	"Salli Voimien k�ytt�",				// allow power-ups
	"Salli Terveyspakkaukset",			// allow health kits
	"Salli Haarniskat",				// allow armor
	"Loputtomat Ammukset",				// unlimited ammunition
	"Esiasetettu FOV",				// fixed field-of-vision
	"Joukkuepeli",					// team-play
	"Satuta Joukkuetovereita",			// hurt teammates
	"Nopea Vaihto",					// fast weapon switch
	"Askeleet",					// footsteps audible
	"Salli koukku",					// allow hook
	"Haulikon Hylsyt",				// show shotgun shells?

	"Palvelimen IP Osoite",				// server internet provider address
	"Paikallisia pelej� ei l�ytynyt",		// no local games found
	"Aloitus Kartta",					// starting map
	"Palvelimen Nimi",				// server name
	"Pelaajan Nimi",				// player name
	"Joukkueen Nimi",				// team name

	"Tee Harakiri?",				// confirm quit

	"Kyll�",					// yes, acknowledgement
	"Ei",						// no, acknowledgement
	"Tallenna peli",				// cek[1-3-00] changed from 'save' to 'save game'

	"Lataa Asetukset",				// cek[12-6-99]
	"Tallenna Asetukset",				// cek[12-6-99]
	"Poista Asetukset",				// cek[12-6-99]
	"Valitse Asetustiedosto",			// cek[12-6-99]
	"Poista valinta?",				// cek[12-6-99]

	"Lataa Peli",					// cek[12-6-99]
	"Haarniska",					// cek[12-6-99]
	"Terveys",					// cek[12-6-99]
	"Taso",						// cek[12-6-99]
	"Hirvi�t",					// cek[12-6-99]
	"Salaisuudet",					// cek[12-6-99]
	"Aika",						// cek[12-6-99]
	"Yhteens�",					// cek[12-6-99]
	"Jakso",					// cek[12-6-99]

	"Ota 3D-��ni k�ytt��n",				// cek[12-6-99]

	"Tallennettu",					// cek[12-7-99]
	"Ladattu",					// cek[12-7-99]
	"Poistettu",					// cek[12-7-99]

	"Tasoita hiirt�",				// cek[12-7-99]
	"Rulla yl�s",					// cek[12-7-99]
	"Rulla alas",					// cek[12-7-99]

	"ALT-TAB poissa k�yt�st�",			// cek[12-7-99]
	"Verta ja teurastusta",				// cek[12-7-99]
	"Aseiden heilunta",				// cek[12-7-99]
	"Aseiden automaattinen vaihto",			// cek[12-7-99]

	"Valovoiman teho",				// cek[12-7-99] cek[12-13-99]	TL Note: Was "Intensity" I've translated it as "luminous intensity". "The brightness of a texture while it's being affected by dynamic lights."
	"Kiilt�v�t aseet",				// cek[12-7-99]
	"Luotireij�t",					// SCG[1/4/00]: 

	"Eteen",					// cek[12-9-99]
	"Sivuttain",					// cek[12-9-99]
	"Yl�s",						// cek[12-9-99]
	"Kallistus",					// cek[12-9-99]
	"K��ntyminen",					// cek[12-9-99]
	"Ota peliohjain k�ytt��n",			// cek[12-9-99]
	"M��rit� akselit",				// cek[12-9-99]
	"M��rit� ohjaimen valinnat",			// cek[12-9-99]
	"Valitse toiminto t�lle painikkeelle",		// cek[12-9-99]
	"Paina ohjaimen painiketta m��ritt��ksesi",	// cek[12-9-99]
	"Kynnysarvo",					// cek[12-9-99]
	"Herkkyys",					// cek[12-9-99]

	"Lipunry�st�",					// cek[12-10-99]
	"Palaa",					// cek[12-10-99]
	"56.6k",					// cek[12-10-99]
	"Automaattinen",				// cek[12-10-99]
	"Joukkue 1",					// cek[12-10-99]
	"Joukkue 2",					// cek[12-10-99]

	"Toista Intro-video",				// cek[12-13-99]
	"K�yt� kykyj�rjestelm��",			// cek[12-13-99]
	"Hiiren Nopeus",				// cek[12-13-99]

	"Apuri NOUDA",					// cek[1-3-00]
	"Apuri SEURAA",					// cek[1-3-00]
	"Apuri HY�KK��",				// cek[1-3-00]
	"Apuri PER��NNY",				// cek[1-3-00]
	"Apuri PAIKKA",					// cek[1-3-00]

	"Valitse Ase %d",				// cek[1-4-00]  as in "Select Weapon 1" (leave the %d)

	"Tilan�kym� Vaihtele",					// cek[1-5-00]  -- for these, translate HUD to something short and reasonably
	"Tilan�kym� Seur.",					// cek[1-5-00]  -- similar in meaning.
	"Tilan�kym� Edel.",					// cek[1-5-00]
	"Tilan�kym� K�yt�",					// cek[1-5-00]

	"Juttele",					// cek[1-11-00]
	"Tulostaulu",					// SCG[1/13/00]: 
	"Juttele joukkueelle",				// cek[1-22-00]
	"%s on jo m��ritty toimintoon %s",		// cek[1-22-00]  as in "F is already assigned to attack" (leave in %s)
	"Korvaa?",					// cek[1-22-00]

	"Toastednet",					// FS: changed to tnet
	"DK News",					// FS: changed to dknews
	"Jatka?",					// cek[2-1-00]

	// for the next two, all \n in the strings denotes a string break.  These must remain in place and the translated
	// string should have them in roughly the same place.
	"Haluatko yhteis�n luomia karttoja, modeja ja muuta kamaa?\nSiirry dk.toastednet.org-sivustolle.\nJatkaminen avaa www-selaimesi.", // FS: changed to tnet
	"Voit halutessasi lukea Daikatana-uutisia\navaamalla www.daikatananews.net.\nSivustolle jatkaminen avaa www-selaimesi.", // FS: changed to dknews			// cek[2-1-00]

	"Ase %d",
	"Peruuta",			// cek[3-9-00]
	"www.daikatananews.net",	// FS: changed to dknews					// cek[3-9-00]
	"Siirry osoitteeseen www.daikatananews.net.\nSivustolle jatkaminen avaa www-selaimesi.",	// cek[3-9-00]
	
	"S��d� Aseita",		// FS
	"V�litt�m�t tapot",	// FS
	"Ivailu",		// FS
	"Rajoittamattomat tallennukset",// FS
	"CTF/DT-vihjeet",		// FS

	"Huomautus Mini-ajureista",	// Knightmare added 2/20/13
	"T�m� Daikatana-julkaisu ei en�� tue\n3DFX ja PowerVR mini-ajureita.\n3DFX-k�ytt�ji� suositellaan asentamaan\nMetabyte WickedGL -ajurit.",	// Knightmare added 2/20/13
	"Lataa WickedGL-ajurit",	// Knightmare added 2/20/13
	"Siirryt��n www.3dfxzone.it.\nEteep�in jatkaminen k�ynnist�� web-selaimesi.",	// Knightmare added 2/20/13
	"Vaihda oletusarvoiseen OpenGL:��n",	// Knightmare added 2/20/13

	"Ruudunkaappaus",		// Knightmare added 1/21/14
	"Hiljainen ruudunkaappaus",	// Knightmare added 1/21/14
	"Jaetut kokemuspisteet",	// Knightmare added 2/1/14
	"Poista Peli",			// Knightmare added 2/7/14
	"Poista pelitallennus?",	// Knightmare added 2/7/14
	"Shift-painike",		// FS: For Mouse and Joystick
	"Olemme k�ytt�neet paljon aikaa korjataksemme\nalkuper�iset kartat, sek� lis�nneet apureihin\nuusia toimintoja kuten terveyden elpymisen.\nPeli on nautittavampi apurien kanssa.", // FS: For no sidekicks
	"Oletko varma, ett� haluat pelata ilman heit�?\n", // FS: For no sidekicks

	"Tehosta Voimaa",		// FS: Added by request
	"Tehosta Hy�kk�yst�",		// FS: Added by request
	"Tehosta Nopeutta",		// FS: Added by request
	"Tehosta Akrobatiaa",		// FS: Added by request
	"Tehosta Elinvoimaa",		// FS: Added by request

	"Painike 4",		// Knightmare 1/30/15- button 4 label
	"Painike 5"		// Knightmare 1/30/15- button 5 label
};


static char *tongue_menu_options[] =
{
	"Aina Juoksussa",	// always run
	"Katsetilan vaakatasaus",	// look-spring
	"Katsetilan sivuttaisastelu", // look-strafe
	"Automaattit�ht�ys",	// auto-targeting
	"T�ht�in",		// crosshair
	"Aseet N�kyvill�",	// weapon visible
	"Tekstitykset",		// subtitles

	"��nitehosteiden ��nenvoimakkuus",	// volume for sound fx
	"Musiikin ��nenvoimakkuus",		// volume for music
	"Elokuvatehosteiden ��nenvoimakkuus",	// volume for cinematics

	"Ohjelmistollinen",		// video mode, software
	"OpenGL",			// video mode, opengl
	"Tallenna Muutokset",		// apply video mode changes

	"T�ysiruutu",			// fullscreen
	"Varjot",			// shadows
	"Ajuri",			// video driver
	"Tarkkuus",			// resolution
	"Ruudun Koko",			// screen size
	"Kirkkaus",			// brightness
	"Pintakuvioiden Laatu",		// texture quality
	"Tilannepalkin N�kyvyys",	// status bar visibility
	"Sumun/Veden Tarkkuus",		// SCG[12/4/99]: 
	"Ronin",			// difficulty level easy
	"Samurai",			// difficulty level medium
	"Shogun",			// difficulty level hard

	"Lumi ja Sade",			// cek[1-11-00]
	"Sumu",				// cek[1-11-00]	

	"Toista Voimaantumis��net",		// FS
	"Toista Puuttuvat ��net",		// FS
	"Toista Apurien Rupattelu��net",	// FS

	"Anisotrooppinen suodatus",	// Knightmare added 5/29/12
	"Vapaavalintainen Leveys",	// Knightmare added 5/29/12
	"Vapaavalintainen Korkeus",	// Knightmare added 5/29/12
	"Tilannen�kym�n koko",		// Knightmare added 6/14/12
	"N�yt�ntahdistus",		// Knightmare added 10/4/12
	"Asynkrooninen virkistys",	// Knightmare added 10/4/12
	"Virkistystaajuus",		// Knightmare added 10/4/12
	"Ruudunkaappauksen muoto",	// Knightmare added 3/11/13
	"T�pl�",			// Knightmare added 5/10/13
	"Tilavuus",			// Knightmare added 5/10/13

	"Lataus Valinnat",		// Knightmare added 11/19/13
	"Salli lataaminen",		// Knightmare added 11/19/13
	"Salli HTTP-lataaminen",	// FS added 03/29/15
	"kartat / pintakuviot",		// Knightmare added 11/19/13
	"��net",			// Knightmare added 11/19/13
	"mallit",			// Knightmare added 11/19/13
	"��nen taajuus",		// Knightmare added 2/2/14
	"��nimoottori",			// Knightmare added 7/8/16
};



// Descriptions for the texture quality settings.
static char *texture_quality_desc[] =
{
	"T�ysi",
	"Puolet",
	"Nelj�sosa",
	"Kahdeksasosa"
};


// weapon names
static char *tongue_weapons[] =
{
	"Ammukset loppu",
	"Ballista",
	"Ballistan tukkinuolia",
	"Pultitin",
	"Pulttinuolia",
	"C4 Vitzatergo",	// TL: Romero's Semi-Fake Latin meaning "Power from Behind" i.e. "virtus a tergo".
	"C4-pakkaus",
	"Daikatana",
	"Daedaluksen kiekko",
	"Kiekko",
	"H�iritsin Hanska",
	"Glock 2020",
	"muutamia luoteja",
	"Haadeksen Vasara",
	"Ionipuhallin",
	"Ionipakkaus",		// SCG[11/28/99]: 
	"Kineettikeski�",
	"Kineettisf��ri",
	"Metamaseri",
	"Metamaseri",
	"Nharren Painajainen",
	"joitakin sis�elimi�",	// SCG[11/28/99]: 
	"Novas�detin",
	"Novakenno",		// SCG[11/28/99]: 
	"Reijitin",
	"Reik�imi�",		// Knightmare changed 8/29/14
	"Aaltoiskin",
	"Shokkipallo",
	"Luotijaksotin",
	"Luotijaksoittimen Hauleja",
	"Sarvikalkkaro",
	"Sarvikalkkaron Raketteja",	// SCG[11/28/99]: 
	"Hopeak�p�l�",
	"Lutkuri",
	"Lutkuja",
	"Stavrosin Sauva",
	"Laava Kivi�",
	"Aurinkoroihu",
	"Aurinkoroihu",
	"Poseidonin Atrain",
	"Atraimenk�rki�",
	"Myrkytin",
	"Kobramyrkky�",		// SCG[11/28/99]: 
	"Wyndraxin Juovain",
	"Juova",
	"Zeuksen Silm�",
	"Mystinen Silm�",
	"Ultimaaliset Moottorik�det",

	// FS: Needed for Configure Weapons Menu
	"Ionipuhallin",
	"C4 Vitzatergo",
	"Shotcycler",
	"Sarvikalkkaro",
	"Aaltoiskin",
	"Deadaluksen kiekko",
	"Myrkytin",
	"Aurinkoroihu",
	"Haadeksen Vasara",
	"Poseidonin Atrain",
	"Zeuksen Silm�",
	"Pultitin",
	"Stavrosin Sauva",
	"Ballista",
	"Wyndraxin Juovain",
	"Nharren Painajainen",
	"Glock 2020",
	"Lutkuri",
	"Kineettikeski�",
	"Reijitin",
	"Novas�detin",
	"Metamaseri",

	// Knightmare 8/29/13- Cordite grenades was stealing ripgun ammo identifier
	"Kordiittikranaatteja"
};


static char *tongue_armors[] =
{
	"Ter�slujitemuovihaarniska",
	"Kromihaarniska",
	"Hopeahaarniska",
	"Kultahaarniska",
	"Rengashaarniska",
	"Adamantiinihaarniska",
	"Kevlarihaarniska",
	"Eboniittihaarniska"
};


// monster names
static char *tongue_monsters[] =
{
	"RoboCo Battle Boar",
	"Musta Vanki",
	"Paiseinen",	// TL: Buboid? Bubo + Huma(/no)id? WTF?
	"RoboCo Cambot",
	"Sadanp��mies",
	"Kerberos",
	"Flying Chaingunner",
	"Caryatid",
	"RoboCo Crox",
	"RoboCo Deathsphere",
	"Tuomiolepakko",
	"Lohik��rme",
	"K��pi�",
	"Femgang",
	"Lautturi",
	"Fletcher",
	"RoboCo Froginator",
	"Kuningas Gharroth",
	"Kultakala",
	"Griffon",
	"Harpyija",
	"Inmater", // TL: Linnakundittaja? Vangittaja? 
	"Kage",
	"Hellfire Paladin",
	"Voltaic Valkyrie",
	"Labra Apina",
	"Cryotech",
	"RoboCo Lasergat",
	"Lycanthir",
	"Meduusa",
	"Mishima Vartija",
	"Nekromantikko Nharre",
	"Rutto Rotta",
	"Putki Rotta",
	"Pappi",
	"Vanki",
	"RoboCo Protopod",
	"Psyclaw",
	"RoboCo Ragemaster 5000",
	"Gang Rocketeer",
	"Navy SEAL Rocketeer",
	"M�t�mato",
	"Satyyri",
	"NAVY SEAL Commando",
	"Navy SEAL Captain",
	"Navy SEAL Shotgunner",
	"Hai",
	"Slaughterskeet",
	"Luuranko",
	"Laiha Ty�l�inen",
	"Lihava Ty�l�inen",
	"RoboCo Sludge Minion",
	"Pieni H�m�h�kki",
	"H�m�h�kki",
	"Velho Stavros",
	"Kirurgi",
	"Varas",
	"RoboCo Thunderskeet",
	"Gang Uzi",
	"White Prisoner",
	"Velho",
	"Velho Wyndrax",
	"Lokki",
	"Tulik�rp�nen",
	"Wisp",
	"Mikiko",
	"Haamu",
	"DopeFish"
};


static char *tongue_world[] =
{
	"j�ljell�",					// triggers to go until sequence complete
	"Sarja suoritettu",				// trigger sequence completed

	"Sinun on omattava",				// need a specific key
	"Sinun on omattava sopiva avain!\n",		// wrong key in possession
	"Can't be opened this way\n",			// SCG[11/1/99]: Can't be opened this way

	"You feel the poison leaving your system.\n",	// poison expiring
	"Your oxylungs are almost used up.\n",		// oxylung expiring
	"Your envirosuit is expiring.\n",		// envirosuit expiring
	"Green Elf needs food badly.\n",		// megashield expiring
	"The Wraith Orb is waning.\n",			// wraith orb expiring
	"Power Boost is running out.\n",		// power boost expiring
	"Attack Boost is running out.\n",		// attack boost expiring
	"Speed Boost is running out.\n",		// speed boost expiring
	"Acrobatic Boost is running out.\n",		// jump boost expiring
	"Vitality Boost is running out.\n",		// health boost expiring

	"liittyi peliin",				// player joined the game
	"poistui kent�st�",				// player exited the level

	"Hiro Miyamoto",				// Hiro Miyamoto's full name
	"Superfly Johnson",				// Superfly's full name
	"Mikiko Ebihara",				// Mikiko's full name

	// SCG[10/18/99]: these next 5 had enums, but no strings. 
	"Sait potkut sp�mm��misest�",			// SCG[10/18/99]: T_PLAYER_SELF_SPAM_KICK // FS: Updated this
	"potkittiin sp�mm��misest�",			// SCG[10/18/99]: T_PLAYER_SPAM_KICK // FS: Updated this
	"",						// SCG[10/18/99]: T_PLAYER_TIME_LIMIT_HIT
	"",						// SCG[10/18/99]: T_PLAYER_FRAG_LIMIT_HIT
	"Tuntematon Pelaaja!",				// SCG[10/18/99]: T_TELL_CMD_UNKNOWN_PLAYER // FS: Updated this

	"Et voi l�hte� alueelta ilman\n",			// SCG[10/18/99]: no exiting level without sidekicks
	"Et voi poistua ilman\n",			// SCG[11/9/99]: No exiting without a key
	"Myrkytetty!\n",				// NSS[11/29/99]: Needed a generic and yet global poisoned message

	"L�ysit salarin!\n",			// cek[1-3-00]
	"Manakallo hupenee.\n",			// cek[2-9-00]
	"Nupitettu!\n",					// FS
	"Teurastus!\n",					// FS
	"Daikatana 1.3:n kehitt�j�t toivottavat hyv�� joulua!\n"	// FS
};

static char *tongue_deathmsg_daikatana[] = 
{
	"%s:n viilsi auki %s\n"
};

static char *tongue_deathmsg_self[] =
{
	"'s skull appears to be split open.\n",	// death, fell
	"sucked it down.\n",			// death, drowned
	"goes crispy.\n",			// death, lava
	"was slimed.\n",			// death, slimed
	"succumbed to the poison.\n",		// death, poison
	"failed at life.\n",			// death, killed self
	"was telefragged.\n",			// death, teleport
	"was compressed into a gib pile.\n",	// death, crushed
	"ate a full load of C4.\n",		// death, C4
	"was frozen solid.\n",			// FS: death, frozen
	"was shocked by lightning.\n"		// FS: death, shocked by lightning bolts in e3
};

static char *tongue_deathmsg_weapon1[] =
{
	"%s was humiliated by %s!\n",
	"%s ultra-smacked %s into oblivion!\n",
	"%s was ionized by %s!\n",
	"%s made a mess of %s!\n",
	"%s double-barrelled %s!\n",
	"%s rode %s's rocket!\n",
	"%s shook up %s!\n"
};

static char *tongue_deathmsg_weapon2[] =
{
	"%s played ultimate frisbee with %s!\n",
	"%s venomized %s!\n",
	"%s turned %s into a burning inferno!\n",
	"%s hammered-down %s!\n",
	"%s punctured %s!\n",
	"%s kneeled before Zeus!\n"
};

static char *tongue_deathmsg_weapon3[] =
{
	"%s was carved like a ham by %s!\n",
	"%s took %s's twig!\n",
	"%s took %s's hardwood!\n",
	"%s was burnt down by %s's meteor!\n",
	"%s handled some of %s's high voltage!\n",
	"%s was done by %s's demon!\n"
};

static char *tongue_deathmsg_weapon4[] =
{
	"%s popped a cap in %s!\n",
	"%s shell-shocked %s!\n",
	"%s was ripped down by %s!\n",
	"%s made a nasty mess of %s!\n",
	"%s was iced by %s!\n",
	"%s burnt a hole in %s!\n",
	"%s was masered by %s!\n"
};

static char *tongue_scoreboard[] =
{
	"Nimi",
	"Tapot",
	"Vasteaika",
	"Aika",
	"Laukaukset",
	"Osumat",
	"Kuolemat",
	"Pisteet"	// cek[1-22-00]
};


static char *tongue_difficulty[] =	// difficulty settings for 'new game' menu
{
	"Valitse Vaikeusaste:",	// difficulty header
	"Ronin",		// newbie
	"Samurai",		// average
	"Shogun"		// expert (or so they think!)
};


static char *tongue_skincolors[] =
{
	"Sininen",	// blue
	"Vihre�",	// green
	"Punainen",	// red
	"Kultainen"	// gold
};


static char *tongue_statbar[] =
{
	"VOIMA",		// skill 'power' label
	"HY�KK�YS",		// skill 'attack' label
	"NOPEUS",		// skill 'speed' label
	"AKROBATIA",			// skill 'acrobatic' label
	"ELINVOIMA",		// skill 'vitality' label

	"HAARNISKA",		// armor label
	"TERVEYS",		// health label
	"AMMUKSET",		// ammo label
	"TAPOT",		// kills label
	"TASO",			// experience points label

	"LEVEL UP!",		// experience level up
	"Valitse",		// select skill
	"Lis�� kykypiste",	// add point to skill

	"LATAA..."		// loading new level
};


static char *tongue_weapon_misc[] =
{
	"C4 Modules are destabilizing!\n"	// C4 modules are de-stabilizing
};


static char *tongue_sidekick[] =	// sidekick commands
{
	"NOUDA",		// 'get' item command
	"TULE",			// 'come here', 'come to me'
	"PAIKOILLASI",		// 'stay' command, 'don't move'
	"HY�KK��",		// 'attack' command
	"PER��NNY",		// 'back off', 'move away'

	"Saisinko"		//  asking to pick up an item: "Can I have the shotgun?"
};

static char *tongue_ctf[] =
{
	"Punainen Lippu",
	"Sininen Lippu",
	"Tuntematon joukkue",

	"%s Lippu",	// when translating, treat '%s Flag' as 'red Flag' and place the %s before or after flag
			// depending on the syntax of the language.  ie if the expression should be 'Flag red' use 'Flag %s'
	"Punainen",
	"Sininen",
	"Krominen",
	"Ter�ksinen",
	"Vihre�",
	"Oranssi",
	"Purppura",
	"Keltainen",

	"Tilanne on nyt: %s:%d, %s:%d\n",
	"You have captured the %s!\n",
	"%s from your team has captured the %s!\n",
	"%s from the %s team has captured your flag!\n",	// when translating, treat '%s team' as 'red team' and place the %s before or after team
								// depending on the syntax of the language.  ie if the expression should be 'team red' use 'team %s'
	"You grabbed the %s\n",
	"%s from your team has grabbed the %s\n",
	"%s has stolen your flag\n",
	"The %s has been returned\n",

	"%d is an invalid team number.  Use 1 or 2\n",
	"You are on team %d (%s)\n",
	"You are already on team %d\n",
	"%s joined the %s team\n",				// translate same as with the %s team above

	"The %s team has won!\nFinal score: %s:%d, %s:%d\n",	// translate same as with the %s team above
	"The game is a tie!\n",
	"Time has expired!\n",
	"The capture limit has been reached!\n",

	"Get ready for overtime!\n",				// FS
	"You have the enemy flag.  Return to base!\n",		// FS
	"The enemy has your flag!  Get the flag back!\n"	// FS
};

static char *tongue_deathtag[] =
{
	"Pommi",						// as in backpack
	"%s Pommi",						// as in red pack
	"The score limit has been reached!\n",
	"Your time is running out!\n",
	"Your time just ran out!\n",

	"%s from the %s team has grabbed the %s!\n",		// translate using the '%s team' guidelines above

	"Your team gets a point!\n",
	"Your team gets %d points!\n",				// you get 2 points
	"The %s team gets a point!\n",				// translate using the '%s team' guidelines above
	"The %s team gets %d points!\n",			// The red team gets 2 points

	"You scored!\n",
	"%s from your team has scored!\n",
	"%s from the %s team has scored!\n",

	"You have the Bomb!  Go score!\n",			// FS
	"The other team has their Bomb!\n"			// FS
};

// FS: --------------------new-------------------------
static char *tongue_save_error[] =
{
	"You must be running a local game to save!\n",
	"You must be in a game to save!\n",
	"You can not save in deathmatch!\n",
	"You can not save during a cinematic!\n",
	"You can not save while dead!\n",
	"You can not save during intermission!\n",
	"You can not save here!\n"
};

